.. _part6:

************************************************************************************************
Partie 6 | Graphes: parcours, arbres sous-tendants, et plus courts chemins
************************************************************************************************

Rendez-vous en S12!

Exercices théoriques: première partie
=======================================

.. note::
   Vous devez faire ces exercices pour le lundi de S13.

Les exercices seront publiés le lundi de S12.

Exercices d'implémentation sur Inginious
==========================================

.. note::
   Vous devez faire ces exercices pour le lundi de S14.

Les exercices seront publiés le lundi de S13.

Exercices théorique: deuxième partie
=======================================

.. note::
   Vous devez faire ces exercices pour le lundi de S14.

Les exercices seront publiés le lundi de S13.