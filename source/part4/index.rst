.. _part4:

************************************************************************************************
Partie 4 | Dictionnaires: tables de hashages et autres implémentations
************************************************************************************************

Rendez-vous en S8!

Exercices théoriques: première partie
=======================================

.. note::
   Vous devez faire ces exercices pour le lundi de S9.

Les exercices seront publiés le lundi de S8.

Exercices d'implémentation sur Inginious
==========================================

.. note::
   Vous devez faire ces exercices pour le lundi de S10.

Les exercices seront publiés le lundi de S9.

Exercices théorique: deuxième partie
=======================================

.. note::
   Vous devez faire ces exercices pour le lundi de S10.

Les exercices seront publiés le lundi de S9.