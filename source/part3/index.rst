.. _part3:

************************************************************************************************
Partie 3 | Arbres de recherche
************************************************************************************************

Rendez-vous en S5!

Exercices théoriques: première partie
=======================================

.. note::
   Vous devez faire ces exercices pour le lundi de S6.

Les exercices seront publiés le lundi de S5.

Exercices d'implémentation sur Inginious
==========================================

.. note::
   Vous devez faire ces exercices pour le lundi de S7.

Les exercices seront publiés le lundi de S6.

Exercices théorique: deuxième partie
=======================================

.. note::
   Vous devez faire ces exercices pour le lundi de S7.

Les exercices seront publiés le lundi de S6.