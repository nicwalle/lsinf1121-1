.. _part5:

************************************************************************************************
Partie 5 | Files de priorités, union-find et compression de données
************************************************************************************************

Rendez-vous en S10!

Exercices théoriques: première partie
=======================================

.. note::
   Vous devez faire ces exercices pour le lundi de S11.

Les exercices seront publiés le lundi de S10.

Exercices d'implémentation sur Inginious
==========================================

.. note::
   Vous devez faire ces exercices pour le lundi de S12.

Les exercices seront publiés le lundi de S11.

Exercices théorique: deuxième partie
=======================================

.. note::
   Vous devez faire ces exercices pour le lundi de S12.

Les exercices seront publiés le lundi de S11.